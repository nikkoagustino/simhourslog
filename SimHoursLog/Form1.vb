﻿
Imports System.Data.SQLite
Imports Newtonsoft.Json.Linq

Public Class Form1
    Dim sqlite_conn As SQLiteConnection
    Dim sqlite_cmd As SQLiteCommand
    Dim sqlite_datareader As SQLiteDataReader
    Dim current_row As String
    Dim start_time As String
    ' Dim base_url As String = "http://localhost/simhourslog"
    Dim base_url As String = "http://logtime.flightdeckindonesia.com"
    Private Sub updateRemoteLog(ByVal last_update_time As String)
        Try
            Dim parameter As String = "simcode=" + lblSimcode.Text + "&row=" + current_row + "&start_time=" + start_time + "&last_update_time=" + last_update_time
            Dim encoded As String = System.Net.WebUtility.UrlEncode(parameter)

            Dim webClient As New System.Net.WebClient
            Dim result As String = webClient.DownloadString(base_url + "/api/remote_log/" + encoded)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub UpdateLastData()
        Dim prev_update_time As String = ""
        Dim prev_row As String = ""
        Dim prev_start_time As String = ""
        Dim prev_id As String = Convert.ToInt32(current_row) - 1
        ' get last insert id
        sqlite_cmd = sqlite_conn.CreateCommand()
        sqlite_cmd.CommandText = "SELECT id, start_time, last_update_time FROM logtime WHERE id = " + prev_id
        sqlite_datareader = sqlite_cmd.ExecuteReader()
        While sqlite_datareader.Read
            prev_row = sqlite_datareader.GetValue(0)
            prev_start_time = sqlite_datareader.GetValue(1)
            prev_update_time = sqlite_datareader.GetValue(2)
        End While

        Try
            Dim parameter As String = "simcode=" + lblSimcode.Text + "&row=" + prev_row + "&start_time=" + prev_start_time + "&last_update_time=" + prev_update_time
            Dim encoded As String = System.Net.WebUtility.UrlEncode(parameter)

            Dim webClient As New System.Net.WebClient
            Dim result As String = webClient.DownloadString(base_url + "/api/remote_log/" + encoded)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub UpdateLocalLog()

        ' update current row on tick
        sqlite_cmd = sqlite_conn.CreateCommand()
        sqlite_cmd.CommandText = "UPDATE logtime SET last_update_time = datetime() WHERE id = " + current_row + ";"
        sqlite_cmd.ExecuteNonQuery()

    End Sub
    Private Sub tmrSendlog_Tick(sender As Object, e As EventArgs) Handles tmrSendlog.Tick
        ' SendLog()
        UpdateLocalLog()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SendLog()

        ' open the connection:
        sqlite_conn = New SQLiteConnection("Data Source=" + lblSimcode.Text + ".sqlite;Version=3;")
        sqlite_conn.Open()
        sqlite_cmd = sqlite_conn.CreateCommand()

        ' create if not exist
        sqlite_cmd.CommandText = "CREATE TABLE IF NOT EXISTS logtime ( 
                                    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                                    start_time TEXT NULL, 
                                    last_update_time TEXT NULL)"
        sqlite_cmd.ExecuteNonQuery()

        ' insert new row on form open
        sqlite_cmd = sqlite_conn.CreateCommand()
        sqlite_cmd.CommandText = "INSERT INTO logtime (start_time, last_update_time) VALUES (datetime(), datetime());"
        sqlite_cmd.ExecuteNonQuery()

        ' get last insert id
        sqlite_cmd = sqlite_conn.CreateCommand()
        sqlite_cmd.CommandText = "SELECT id, start_time FROM logtime ORDER BY id DESC LIMIT 1"
        sqlite_datareader = sqlite_cmd.ExecuteReader()
        While sqlite_datareader.Read
            current_row = sqlite_datareader.GetValue(0)
            start_time = sqlite_datareader.GetValue(1)
            '            Console.WriteLine(sqlite_datareader.GetValue(0))
        End While

        UpdateLastData()
    End Sub


    Private Sub tmrRemoteLog_Tick(sender As Object, e As EventArgs) Handles tmrRemoteLog.Tick
        Dim time_update As String = "0000-00-00 00:00:00"
        ' get last insert id
        sqlite_cmd = sqlite_conn.CreateCommand()
        sqlite_cmd.CommandText = "SELECT last_update_time FROM logtime WHERE id = " + current_row
        sqlite_datareader = sqlite_cmd.ExecuteReader()
        While sqlite_datareader.Read
            time_update = sqlite_datareader.GetValue(0)
            Console.WriteLine(sqlite_datareader.GetValue(0))
        End While

        updateRemoteLog(time_update)
    End Sub
End Class
